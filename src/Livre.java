import java.util.ArrayList;
import java.util.List;

public class Livre {
	private int code;
	private String auteur;
	public String titre;
	private static List<Livre> listlivre = new ArrayList<Livre>();
	public Livre (int c, String aut, String tit)
	{
		boolean b=true;
		for (Livre l: listlivre)
		{
			if (c == l.code)
			{
				b=false;
				break;
			}
						
		}
		if (b)
		{
			this.code = c;
			this.titre = tit;
			this.auteur= aut;
			listlivre.add(this);
		}
		else
		{
			System.out.println("code existant!");
		}
	}
	public int getCode() {
		return code;
	}
	
	public static List<Livre> getListlivre() {
		return listlivre;
	}
	public static void setListlivre(List<Livre> listlivre) {
		Livre.listlivre = listlivre;
	}
	public String getAuteur() {
		return auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	@Override
	public String toString() {
		return "Livre [code=" + code + ", auteur=" + auteur + ", titre=" + titre + "]";
	}
	public static String Recherchetit (int c)
	{
		for (Livre l: listlivre)
		{
			if (c==l.code)
				return l.titre;
		}
		return null;		
	}
	public static Livre Rechercheliv (int c)
	{
		for (Livre l: listlivre)
		{
			if (c==l.code)
				return l;
		}
		return null;		
	}
	public void supp(){
	listlivre.remove(this);}
}
