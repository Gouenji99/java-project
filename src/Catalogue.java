import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Catalogue {
	private boolean [][] index;
	private String tabM [] ;
	private int tabC [] ; 
	private int numcat;
	private static List<Catalogue> listcatal = new ArrayList<Catalogue>();
	private List<Livre> livcatal = new ArrayList<Livre>();
	private int longM=0,longC=0;
	
	public Catalogue (int numcat) 
	{
		
		this.numcat = numcat;
		tabM = new String[100];
		tabC = new int [100];
		longC=0;
		longM=0;
		listcatal.add(this);
		
	}
	public void Ajout (Livre l)
	{
		livcatal.add(l);
	}
	public void Supprimer (Livre l)
	{
		livcatal.remove(l);
		l.supp();
	}
	public void Indexcatlogue ()
	{	
		boolean trouver;
		int j;
		String aux2="",ch1;
		String tit;
		ArrayList <String> Motvide = new ArrayList <String> (Arrays.asList ("le", "la", "les", "un", "une", "des", "de", "du", "ce", "cette", "ces","avec", "et", "en","sans", "dans", "sur", "par", "�", "au", "aux"));
		for (Livre l: livcatal)
		{	
			int i=0,longt=0;
			tit= l.getTitre();
			tabC[longC++]=l.getCode();
			String[] t2 = tit.split(" ");
			String[] t = new String[100];
			for (String ch: t2)
			{ trouver=false;
					for (String Mot: Motvide)
					{
					if (Mot.equals(ch))
					{
						trouver=true;
							break;
					}	
					}
				if(!trouver)
				{t[longt++]=ch;}
			}
			for (int k=0;k<longt;k++)
			{ trouver=false;
			for( j=0;j<longM;j++)
				if ( t[k].equals(tabM[j]))
				{
					 trouver=true;
					 break;
				}
					if(!trouver)
				tabM[longM++]=t[k];
				
			}
			
		}
		  index = new boolean [longM][longC] ;		
		for (int i=0;i<longM;i++)
		{
			for (j=0; j<longC;j++)
			{
				index [i][j]=false;
				String [] s = Livre.Recherchetit(tabC[j]).split(" ");
				for (int k=0;k<s.length && s[k]!=null;k++)
					{
					if (s[k].equals(tabM[i]))
						{
						index [i][j]=true;
						break;
						}
					}
			}
		}
		 System.out.print("\n   \t\t ");
		 for (int c = 0; c < longC; c++) 
			{System.out.print(" C"+tabC[c]+" ");}
		for (int i = 0; i < longM; i++) {
			if(tabM[i].length()<8)
				aux2="\t";
			 System.out.print("\n"+tabM[i]+"\t"+aux2+"|");
			 aux2="";
		    for (j = 0; j < longC; j++) {
		    	if(index[i][j])
		        System.out.print(" 1 |");
		    	else
		    		 System.out.print(" 0 |");
		    }
		   
		}
		System.out.println();
	}
	public static List<Catalogue> getListcatal() {
		return listcatal;
	}
	public static void setListcatal(List<Catalogue> listcatal) {
		Catalogue.listcatal = listcatal;
	}
	public List<Livre> getLivcatal() {
		return livcatal;
	}
	public void setLivcatal(List<Livre> livcatal) {
		this.livcatal = livcatal;
	}
	public void Recherche_auteur (String auteur )
	{ boolean b=false;
		for (Livre l: livcatal)
		{
			if (auteur.equals(l.getAuteur()))
			{
				System.out.println(l);
				b=true;
			}
		}
		if (b==false)
			System.out.println ("votre auteur n'existe pas dans le catalogue choisi!");
	}
	public void Recherche_req_booleen (String req)
	{
		boolean rep=true;
		int j;	
		if (req.indexOf("|")!=-1)
			
		{
			String t1 [] = req.split(" | ");
			int t2 [] = new int [t1.length];
			for (int i=0 ; i<t2.length;i++)
			{
				for (j=0;j<longM;j++)
				{
				
					if (t1[i].equals(tabM[j]))
					{ 
						t2[i]=j;
						break;					
					}					
				}
				if (!t1[i].equals(tabM[j]))
				{
					t2[i]=-1;
				}
			}
			for (int k=0;k<longC;k++)
			{	rep=false;
				for (int i=0;i<t2.length;i++)
				{
					if (t2[i]!=-1)
					{
						rep=rep || index[t2[i]][k];
					}
				}
				if (rep)
					System.out.println(Livre.Rechercheliv(tabC[k]));
			}
		}
		if (req.indexOf("&")!=-1)
		{
			String t1 [] = req.split(" & ");
			int t2 [] = new int [t1.length];
			for (int i=0 ; i<t1.length;i++)
			{
				for (j=0;j<tabM.length;j++)
				{
					if (t1[i].equals(tabM[j]))
					{
						t2[i]=j;
						break;
					}					
				}
				if (!t1[i].equals(tabM[j]))
				{
					rep=false;
					break;
				}
			}
			if (rep)
			{
				for (int k=0;k<longC;k++)
					{	
					rep=true;
					for (int i=0;i<t2.length;i++)
						{		
							rep=rep && index[t2[i]][k];
						}					
				if (rep)
					System.out.println(Livre.Rechercheliv(tabC[k]));
					}
			
			}
		}
	}
	public void Recherche_req_uniterme (String m)
	{
		
		for (int i=0; i<longM;i++)
		{
			if (m.equals(tabM[i]))
			{
				for (int j=0; j<longC;j++)
				if ((index[i][j])==true)
				{
					
					System.out.println(Livre.Rechercheliv(tabC[j]));
				}					
			}
				
		}							
	}
	public static Catalogue Recherchecatal (int numcat)
	{
		for (Catalogue C: listcatal)
		{
			if (numcat==C.numcat)
				return C;
		}
		return null;		
	}
	public Livre Rechercheliv (int c)
	{
		for (Livre l: this.livcatal)
		{
			if (c==l.getCode())
				return l;
		}
		return null;		
	}
}
	
	
